package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.ModuleApi;
import com.sd365.permission.centre.entity.Module;
import com.sd365.permission.centre.pojo.dto.ModuleDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.query.ModuleQuery;
import com.sd365.permission.centre.pojo.vo.ModuleVO;
import com.sd365.permission.centre.service.ModuleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author lenovo
 * @date 2022/07/081206
 **/
@RestController
public class ModuleController implements ModuleApi {

    @Resource
    ModuleService moduleService;

    /**
     * 通用查询，带分页
     *
     * @param moduleQuery 封装的模块信息
     * @return
     */
    @Override
    @ApiLog
    public List<ModuleVO> commonQuery(ModuleQuery moduleQuery) {
        List<Module> modules = moduleService.commonQuery(moduleQuery);
        List<ModuleVO> moduleVOS;
        try {
            moduleVOS = BeanUtil.copyList(modules, ModuleVO.class);
            System.out.println("controller层commonQuery");
        } catch (Exception e) {
            throw new BusinessException(CommonErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return moduleVOS;
    }

    /**
     * 批量更新
     *
     * @param moduleDTOS
     * @return
     */
    @Override
    @ApiLog
    public Boolean batchUpdate(@RequestBody List<ModuleDTO> moduleDTOS) {
        Boolean flag = true;
        // 批量删除
        for (int i = 0; i < moduleDTOS.size(); ++i) {
            ModuleDTO dto = moduleDTOS.get(i);
            flag = moduleService.batchModify(dto);
            if (flag == false) {
                return false;
            }
        }
        return flag;
    }

    @Override
    public Boolean add(ModuleDTO moduleDTO) {
        return moduleService.add(moduleDTO);
    }

    @Override
    @ApiLog
    public Boolean remove(Long id, Long version) {
        try {
            return moduleService.remove(id, version);
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    @ApiLog
    public Boolean removeBatch(List<IdVersionQuery> idVersionQueryList) {
        try {
            return moduleService.removeBatch(idVersionQueryList);
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public Boolean modify(@Valid ModuleDTO moduleDTO) {
        return moduleService.modify(moduleDTO);
    }

    @Override
    @ApiLog
    public ModuleVO queryModuleById(@Valid @NotNull Long id) {
        ModuleDTO moduleDTO = moduleService.queryById(id);
        if (moduleDTO != null) {
            ModuleVO moduleVO = BeanUtil.copy(moduleDTO, ModuleVO.class);
            System.out.println(moduleVO);
            return moduleVO;
        } else {
            return null;
        }
    }

    @Override
    @ApiLog
    public Boolean status(@RequestBody ModuleDTO moduleDTO) {
        return moduleService.batchModify(moduleDTO);
    }
}
