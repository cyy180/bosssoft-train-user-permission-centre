package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.DepartmentApi;
import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.pojo.dto.DepartmentDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.vo.CompanyVO;
import com.sd365.permission.centre.pojo.vo.DepartmentVO;
import com.sd365.permission.centre.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author jxd
 * @Date 2020/12/12  1:05 下午
 * @Version 1.0
 * @Write For DepartmentController
 * @Email waniiu@126.com
 */
@Slf4j
@RestController
public class DepartmentController implements DepartmentApi {

    @Resource
    DepartmentService departmentService;

    @ApiLog
    @Override
    public List<DepartmentVO> commonQuery(@Valid DepartmentQuery departmentQuery) {
        if (null == departmentQuery) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<DepartmentDTO> departmentDTOS = departmentService.commonQuery(departmentQuery);
        List<DepartmentVO> departmentVOS = BeanUtil.copyList(departmentDTOS, DepartmentVO.class);
        return departmentVOS;
    }


    @ApiLog
    @Override
    public Boolean add(@Valid DepartmentDTO departmentDTO) {

        log.info("name:" + departmentDTO.getCreator());
        return departmentService.add(departmentDTO);
    }

    @ApiLog
    @Override
    public Boolean update(@Valid DepartmentDTO departmentDTO) {
        return departmentService.modify(departmentDTO);
    }

    @ApiLog
    @Override
    public Boolean deleteOneById(Long id, Long version) {
        return departmentService.remove(id, version);
    }

    @ApiLog
    @Override
    public DepartmentVO queryDepartmentById(@Valid @NotNull Long id) {
        //throw new BusinessException(CommonErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("测试异常"));
        DepartmentDTO departmentDTO = departmentService.queryById(id);
        if (departmentDTO != null) {
            DepartmentVO departmentVO = BeanUtil.copy(departmentDTO, DepartmentVO.class);
            return departmentVO;
        } else {
            return null;
        }
    }

    @Override
    public Boolean deleteBatch(List<IdVersionQuery> batchQuery) {
        return departmentService.deleteBatch(batchQuery);
    }

    @Override
    public DepartmentDTO copy(Long id) {
        return departmentService.copy(id);
    }


    @Override
    @ApiLog
    public List<DepartmentVO> commonQueryDepartment(DepartmentQuery departmentQuery) {
        List<Department> departments = departmentService.commonQueryDepartment(departmentQuery);
        List<DepartmentVO> departmentVOS = BeanUtil.copyList(departments, DepartmentVO.class, new BeanUtil.CopyCallback() {
            @Override
            public void copy(Object o, Object o1) {
                Department department = (Department) o;
                DepartmentVO departmentVO = (DepartmentVO) o1;
                if (department.getCompany() != null) {
                    CompanyVO copy = BeanUtil.copy(department.getCompany(), CompanyVO.class);
                    departmentVO.setCompanyVO(copy);
                }
            }
        });
        return departmentVOS;
    }

    @Override
    @ApiLog
    public Boolean batchUpdate(@RequestBody List<DepartmentDTO> departmentDTOS) {
        System.out.println("batchUpdate");
        Boolean flag = true;
        // 批量删除
        for (int i = 0; i < departmentDTOS.size(); ++i) {
            DepartmentDTO dto = departmentDTOS.get(i);
            flag = departmentService.batchModify(dto);
            if (flag == false) {
                return false;
            }
        }
        return flag;
    }

    /**
     * 更新单条启用状态
     *
     * @param departmentDTO 部门DTO
     * @return
     * @author Yan Huazhi
     * @date 2020/12/18 9:20
     * @version 0.0.1
     */
    @Override
    @ApiLog
    public Boolean status(@RequestBody DepartmentDTO departmentDTO) {
        return departmentService.batchModify(departmentDTO);
    }
    @ApiLog
    @Override
    public List<DepartmentDTO> builderTree(DepartmentQuery departmentQuery) {
        return departmentService.builderTree(departmentQuery);
    }
}
