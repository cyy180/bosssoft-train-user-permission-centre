package com.sd365.permission.centre.api.controller;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSON;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.permission.centre.api.LoginApi;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.service.CompanyService;
import com.sd365.permission.centre.service.LoginService;
import com.sd365.permission.centre.service.UserOnlineService;
import com.sd365.permission.centre.service.util.Md5Utils;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * @description: 退出登录修复
 * @author: wujiandong
 * @create: 2022/8/5 17:15
 * @version: v1.0.9
 **/
@RestController
@Slf4j
public class LoginController  implements LoginApi {
    /**
     * 登录服务
     */
    @Autowired
    private LoginService loginService;
    /**
     * 公司服务
     */
    @Autowired
    private CompanyService companyService;
    /**
     * 用户在线服务
     */
    @Autowired
    private UserOnlineService userOnlineService;
    /**
     * 需要从缓存读取
     */
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * redis缓存的token key
     */
    private static final String USER_TOKEN_KEY = "user:token:";
    /**
     * redis缓存的i用户登录信息 key
     */
    private static final String USER_LOGING_KEY = "user:loginInfo:";

    // TODO 这里完成认证
    @Override
    public UserVO auth(@RequestParam(value = "code") String code, @RequestParam(value = "password") String password, @RequestParam(value = "account") String account) {
        /**
         * 1 认证通过返回带权限的UserVO
         * 2 插入登录记录并且更新登录状态
         */

        return null;
    }

    // TODO 这里完成认证成功成功 用户资源等的获取
    @Override
    public UserVO info(@RequestParam(value = "code") String code, @RequestParam(value = "account") String account) {
        return null;

    }
    @Override
    public UserVO logout() {
        String logoutId = BaseContextHolder.getUserIdStr();
        userOnlineService.forceLogout(logoutId);
        UserVO userVO = new UserVO();
        userVO.data = new ResponseData();
        userVO.setCode(HttpStatus.HTTP_OK);
        return userVO;
    }
}
