package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.SubSystemApi;
import com.sd365.permission.centre.entity.Tenant;
import com.sd365.permission.centre.pojo.dto.SubSystemDTO;
import com.sd365.permission.centre.pojo.query.SubSystemQuery;
import com.sd365.permission.centre.pojo.vo.SubSystemVO;
import com.sd365.permission.centre.service.SubSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
public class SubSystemController extends AbstractController implements SubSystemApi {

    @Autowired
    private SubSystemService subSystemService;

    @Override
    public Boolean add(@Valid SubSystemDTO subSystemDTO) {
        return subSystemService.add(subSystemDTO);
    }

    @Override
    public Boolean remove(Long id, Long version) {
        return subSystemService.remove(id,version);
    }

    @Override
    public Boolean batchDelete(@Valid SubSystemDTO[] subSystemDTOS) {
        return subSystemService.batchDelete(subSystemDTOS);
    }

    @Override
    public Boolean modify(@Valid SubSystemDTO subSystemDTO) {
        if (subSystemService.modify(subSystemDTO) != null) {
            return true;
        } else {
            return false;
        }
    }

    @ApiLog
    @Override
    public List<SubSystemVO> commonQuery(SubSystemQuery subSystemQuery) {
        if(null==subSystemQuery){
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        List<SubSystemDTO> subSystemDTOS= subSystemService.commonQuery(subSystemQuery);
        List<SubSystemVO> subSystemVOS=BeanUtil.copyList(subSystemDTOS, SubSystemVO.class);
        return subSystemVOS;
    }

    @ApiLog
    @Override
    public SubSystemVO querySubSystemById(@Valid @NotNull Long id) {
        SubSystemDTO subSystemDTO= subSystemService.querySubSystemById(id);
        if(subSystemDTO!=null){
            SubSystemVO subSystemVO= BeanUtil.copy(subSystemDTO, SubSystemVO.class);
            return subSystemVO;
        }else{
            return null;
        }
    }

    @ApiLog
    @Override
    public List<SubSystemVO> querySubSystemByTenantId(@NotNull Long id) {
        List<SubSystemDTO> subSystemDTOS= subSystemService.querySubSystemByTenantId(id);
        if(subSystemDTOS!=null){
            List<SubSystemVO> subSystemVOS=BeanUtil.copyList(subSystemDTOS, SubSystemVO.class);
            return subSystemVOS;
        }else{
            return null;
        }
    }

    @Override
    public List<Tenant> tenantQuery() {
        List<Tenant> tenants= subSystemService.tenantQuery();
        return tenants;
    }
}
