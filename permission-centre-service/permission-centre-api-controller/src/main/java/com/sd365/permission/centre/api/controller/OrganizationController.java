package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.OrganizationApi;

import com.sd365.permission.centre.pojo.dto.OrganizationDTO;
import com.sd365.permission.centre.pojo.query.OrganizationQuery;
import com.sd365.permission.centre.pojo.vo.OrganizationStructureVO;
import com.sd365.permission.centre.pojo.vo.OrganizationVO;
import com.sd365.permission.centre.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @className: OrganizationController
 * @description: 组织Controller
 * @author: 何浪
 * @date: 2020/12/11 17:27
 */
@RestController
public class OrganizationController extends AbstractController implements OrganizationApi {
    @Autowired
    private OrganizationService organizationService;

    @Override
    public Boolean add(@Valid OrganizationDTO organizationDTO) {
        return organizationService.add(organizationDTO);
    }

    @Override
    public Boolean remove(Long id, Long version) {
        return organizationService.remove(id, version);
    }

    @Override
    public Boolean batchRemove(@Valid OrganizationDTO[] organizationDTOS) {
        return organizationService.batchDelete(organizationDTOS);
    }

    @Override
    public Boolean modify(@Valid OrganizationDTO organizationDTO) {
        return organizationService.modify(organizationDTO);
    }

    @Override
    public Boolean batchUpdate(@Valid OrganizationDTO[] organizationDTOS) {
        return organizationService.batchUpdate(organizationDTOS);
    }

    @Override
    public OrganizationDTO copy(Long id) {
        return organizationService.copy(id);
    }

    @Override
    @ApiLog
    public List<OrganizationVO> commonQuery(OrganizationQuery organizationQuery) {
        if (null == organizationQuery) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<OrganizationDTO> organizationDTOS = organizationService.commonQuery(organizationQuery);
        List<OrganizationVO> organizationVOS = BeanUtil.copyList(organizationDTOS, OrganizationVO.class);
        return organizationVOS;
    }

    @Override
    public OrganizationVO queryOrganizationById(Long id) {
        OrganizationDTO organizationDTO = organizationService.queryById(id);
        if (null != organizationDTO) {
            OrganizationVO organizationVO = BeanUtil.copy(organizationDTO, OrganizationVO.class);
            return organizationVO;
        } else {
            return null;
        }
    }

    @Override
    public List<OrganizationStructureVO> structureVoQueryById( Long id){
        return organizationService.structureVoQueryById(id);
    }
}
