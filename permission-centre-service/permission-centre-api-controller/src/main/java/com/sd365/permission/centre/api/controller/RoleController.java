package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.RoleApi;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.dto.*;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.*;
import com.sd365.permission.centre.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
/**
 * @Class RoleController
 * @Description 角色管理api 主要包含 角色的增删除改查以及角色和资源绑定，角色和用户绑定
 * @Author Administrator
 * @Date 2023-02-26  10:15
 * @version 1.0.0
 * TODO 负责模块的开发的人员需要开启注释代码实现接口
 */
@CrossOrigin
@RestController
public class RoleController /**implements RoleApi**/ {
}
