package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.MessageApi;
import com.sd365.permission.centre.entity.Message;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.MessageDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.MessageQuery;
import com.sd365.permission.centre.pojo.vo.MessageVO;
import com.sd365.permission.centre.service.MessageService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * @author WangHaitang
 * @date 2022/07/161717
 **/
@RestController
public class MessageController implements MessageApi {

    @Resource
    MessageService messageService;

    /**
     * 通用查询
     * @param messageQuery
     * @return
     */
    @Override
    @ApiLog
    public List<MessageVO> commonQuery(MessageQuery messageQuery) {
        List<Message> messages = messageService.commonQuery(messageQuery);
        List<MessageVO> messageVOS;
        try {
            messageVOS = BeanUtil.copyList(messages, MessageVO.class);
            System.out.println("controller层commonQuery");
        } catch (Exception e) {
            throw new BusinessException(CommonErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return messageVOS;
    }

    @Override
    public Boolean add(@Valid MessageDTO messageDTO) {
        return messageService.add(messageDTO);
    }
    @Override
    public Boolean remove(Long id, Long version) {
        return messageService.remove(id,version);
    }

    @Override
    public Boolean modify(@Valid @RequestBody MessageDTO messageDTO) {
        return messageService.modify(messageDTO);
    }

    @Override
    public Boolean batchRemove(@Valid MessageDTO[] messageDTOS) {
        return messageService.batchDelete(messageDTOS);
    }

    @Override
    public Boolean batchUpdate(@Valid MessageDTO[] messageDTOS) {
        return messageService.batchUpdate(messageDTOS);
    }
}
