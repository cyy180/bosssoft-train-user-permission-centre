/**
 * Copyright (C), 2001-${YEAR}, www.bosssof.com.cn
 * @FileName ResourceDTO.java
 * @Author Administrator
 * @Date 2022-9-28  11:06
 * @Description 用于接收前端的资源管理模块 增加和修改请求的资源对象
 * History:
 * <author> Administrator
 * <time> 2022-9-28  11:06
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Class ResourceDTO
 * @Description 对应前端的增加界面提交的对象或者其他服务对我放增加和修改接口调用传入的对象
 * @Author Administrator
 * @Date 2022-9-28  11:08
 * @version 1.0.0
 */
@ApiModel(value="com.sd365.permission.centre.entity.Resource")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceDTO extends TenantBaseDTO {
  /**
  * 存储父亲节点的资源
  */
   private ResourceDTO resourceDTO;
   /**
     * 资源所属的系统
     */
    @Transient
    private SubSystemDTO subSystem=new SubSystemDTO();

    /**
     * 资源名称  注意这个也可以是界面上的按钮
     */
    @ApiModelProperty(value="name资源名称  注意这个也可以是界面上的按钮")
    @NotNull(message = "资源名称不可以为空")
    private String name;

    /**
     * 子系统ID，预留
     */
    @ApiModelProperty(value="subSystemId子系统ID，预留")
    @NotNull(message = "子系统不可以为空")
    private Long subSystemId;

    /**
     * 编号
     */
    @ApiModelProperty(value="code编号 ")
    @NotNull(message = "资源编号不可以为空")
    private String code;

    /**
     * 顺序号，控制菜单显示顺序
     */
    @ApiModelProperty(value="orderIndex顺序号，控制菜单显示顺序")
    @NotNull(message = "资源顺序号不可以为空")
    private Integer orderIndex;

    /**
     * 父亲节点 顶层节点的父亲节点会选择子系统
     */
    @ApiModelProperty(value="parentId父亲节点，顶层节点的父亲节点是子系统")
    private Long parentId;

    /**
     * URL 这个决定加载什么组件 对应import组件
     */
    @ApiModelProperty(value="urlURL 这个决定加载什么组件 对应import组件")
    private String url;
    /**
     * 后端接口地址读音组件的 path属性
     */
    @ApiModelProperty(value="api后端接口地址读音组件的 path属性")
    private String api;

    /**
     * 请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE
     */
    @ApiModelProperty(value="method请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE")
    private Byte method;

    /**
     * 打开图标 这个只能取系统预定义的
     */
    @ApiModelProperty(value="openImg打开图标 这个只能取系统预定义的")
    private String openImg;

    /**
     * 关闭图标  这个只能取系统预定义的
     */
    @ApiModelProperty(value="closeImg关闭图标  这个只能取系统预定义的")
    private String closeImg;

    /**
     * 资源类型  这个是菜单还是按钮 0 菜单 1 按钮
     */
    @ApiModelProperty(value="resourceType资源类型  这个是菜单还是按钮 0 菜单 1 按钮  ")
    @NotNull(message = "资源类型不可以为空")
    private Byte resourceType;

    /**
     * 叶子节点
     */
    @ApiModelProperty(value="leaf叶子节点")
    @NotNull(message = "是否叶子节点选项不可以为空")
    private Byte leaf;

    /**
     * 动作 这个暂时保留
     */
    @ApiModelProperty(value="action动作 这个暂时保留")
    private Byte action;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 公司
     */
    @ApiModelProperty(value="company")
    private CompanyDTO company=new CompanyDTO();

    /**
     * 机构
     */
    @ApiModelProperty(value="organization")
    private OrganizationDTO organization=new OrganizationDTO();;

    @ApiModelProperty(value="tenant")
    private TenantDTO tenant=new TenantDTO();


}
