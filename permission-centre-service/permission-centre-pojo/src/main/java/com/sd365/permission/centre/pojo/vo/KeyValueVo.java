package com.sd365.permission.centre.pojo.vo;

import lombok.Data;

/**
 * @Version :
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.pojo.vo
 * @NAME: KeyVo
 * @author:xuandian
 * @DATE: 2022/7/13 13:23
 * @description: 缓存监控 key
 */
@Data
public class KeyValueVo {
    private String key;
    private Object value;
}
