package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: Organization
 * @description: 组织DTO
 * @author: 何浪
 * @date: 2020/12/11 17:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationDTO extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 编号
     */
    private String code;

    /**
     * 负责人
     */
    private String master;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 地址
     */
    private String address;
}
