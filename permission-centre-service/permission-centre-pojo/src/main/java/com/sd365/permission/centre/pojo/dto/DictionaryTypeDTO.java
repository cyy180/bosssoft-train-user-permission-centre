package com.sd365.permission.centre.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity + Version 注解会将 SQL 自动加上 version
 * */
@Entity
@ApiModel(value="com.sd365.permission.centre.entity.DictionaryType")
@Table(name = "basic_dictionary_type")
public class DictionaryTypeDTO  extends TenantDTO {
    /**
     * 类型名
     */
    @ApiModelProperty(value="name类型名")
    private String name;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 类型名
     */
    public String getName() {
        return name;
    }

    /**
     * 类型名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
