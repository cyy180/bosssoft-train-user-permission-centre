package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value="com.sd365.permission.centre.pojo.dto.DeleteSystemParamDTO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteSystemParamDTO extends TenantBaseDTO {
    private Long id;
    private Long version;
}
