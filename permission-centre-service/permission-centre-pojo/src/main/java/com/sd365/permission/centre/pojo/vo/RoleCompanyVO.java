package com.sd365.permission.centre.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: sd365-permission-centre
 * @description: RoleCompanyVO类
 * @author: wujiandong
 * @create: 2022-08-29
 **/
@ApiModel(value="角色公司管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleCompanyVO {
    /**
     * 角色ID
     */
    @ApiModelProperty(value = "roleId角色ID")
    private Long roleId;

    /**
     * 授权公司ID
     */
    @ApiModelProperty(value = "authCompanyIds授权公司ID列表")
    private List<Long> authCompanyIds;
}
