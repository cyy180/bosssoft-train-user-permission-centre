/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName RoleResourceDTO.java
 * @Author Administrator
 * @Date 2022-9-28  17:41
 * @Description TODO
 * History:
 * <author> Administrator
 * <time> 2022-9-28  17:41
 * <version> 1.0.0
 * <desc> 该文件定义了前端页面在执行资源分配的时候传送的资源对象，支持多角色统一多资源
 */
package com.sd365.permission.centre.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
/**
 * @Class RoleResourceDTO
 * @Description 重点字段是roleIds和resourceIds 系统将角色资源关系插入basic_role_resource表
 * @Author Administrator
 * @Date 2022-9-28  17:43
 * @version 1.0.0
 */
@ApiModel(value="角色资源关系分配传输对象")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RoleResourceDTO extends TenantBaseDTO {
    /**
     * 角色id列表 这个用于插入的角色id列表
     */

    @ApiModelProperty(value="被选中分配资源的角色")
    @NotNull(message = "被选中分配资源的角色")
    private List<Long> roleIds;
    /**
     * 资源id列表 这个是用于插入的资源id列表
     */
    @ApiModelProperty(value="被选中分配给角色的资源")
    @NotNull(message = "被选中分配给角色的资源")
    private List<Long> resourceIds;
    /**
     * 资源表
     */
    @ApiModelProperty(value="被选中分配给角色的资源")
    private List<ResourceDTO> resources;
    /**
     * 角色ID
     */
    @ApiModelProperty(value="roleId角色ID")
    private Long roleId;

    /**
     * 资源ID
     */
    @ApiModelProperty(value="resourceId资源ID")
    private Long resourceId;

    /**
     *  机构
     */
    private OrganizationDTO organizationDTO=new OrganizationDTO();
    /**
     *  公司
     */
    private CompanyDTO companyDTO=new CompanyDTO();

    /**
     * 客户
     */
    private  TenantDTO tenantDTO=new TenantDTO();

}
