package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: sd365-permission-centre
 * @description: RoleResourceQuery类
 * @author: Liwenhai
 * @create: 2020-12-11 21:24
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleResourceQuery extends TenantBaseQuery {
    /**
     * 角色ID
     */
    @ApiModelProperty(value="roleId角色ID")
    private Long roleId;

    /**
     * 资源ID
     */
    @ApiModelProperty(value="resourceId资源ID")
    private Long resourceId;
}
