package com.sd365.permission.centre.pojo.query;

/**
 * @author Yan Huazhi
 * @date 2020/12/15 21:34
 * @version: 0.0.1
 * @description 包含id和version的Query对象，多用于删除
 */
public class IdVersionQuery {
    private Long Id;

    private Long version;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
