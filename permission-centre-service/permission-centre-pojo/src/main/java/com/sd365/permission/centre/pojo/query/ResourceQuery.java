package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import com.sd365.permission.centre.pojo.dto.SubSystemDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;

@ApiModel(description ="资源")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceQuery extends TenantBaseQuery {
    private Long level;

    /**
     * 资源所属的系统
     */
    @Transient
    private SubSystemDTO subSystem;
    /**
     * 资源名称  注意这个也可以是界面上的按钮
     */
    @ApiModelProperty(value="name资源名称  注意这个也可以是界面上的按钮")
    private String name;

    @ApiModelProperty(value = "资源id")
    private Long id;

    /**
     * 子系统ID，预留
     */
    @ApiModelProperty(value="subSystemId子系统ID，预留")
    private Long subSystemId;

    /**
     * 编号
     */
    @ApiModelProperty(value="code编号 ")
    private String code;

    /**
     * 顺序号，控制菜单显示顺序
     */
    @ApiModelProperty(value="orderIndex顺序号，控制菜单显示顺序")
    private Integer orderIndex;

    /**
     * 父亲节点
     */
    @ApiModelProperty(value="parentId父亲节点")
    private Long parentId;

    /**
     * URL 这个决定加载什么组件 对应import组件
     */
    @ApiModelProperty(value="urlURL 这个决定加载什么组件 对应import组件")
    private String url;

    /**
     * 后端接口地址读音组件的 path属性
     */
    @ApiModelProperty(value="api后端接口地址读音组件的 path属性")
    private String api;

    /**
     * 请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE
     */
    @ApiModelProperty(value="method请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE")
    private Byte method;

    /**
     * 打开图标 这个只能取系统预定义的
     */
    @ApiModelProperty(value="openImg打开图标 这个只能取系统预定义的")
    private String openImg;

    /**
     * 关闭图标  这个只能取系统预定义的
     */
    @ApiModelProperty(value="closeImg关闭图标  这个只能取系统预定义的")
    private String closeImg;

    /**
     * 资源类型  这个是菜单还是按钮 0 菜单 1 按钮
     */
    @ApiModelProperty(value="resourceType资源类型  这个是菜单还是按钮 0 菜单 1 按钮  ")
    private Byte resourceType;

    /**
     * 叶子节点
     */
    @ApiModelProperty(value="leaf叶子节点")
    private Byte leaf;

    /**
     * 动作 这个暂时保留
     */
    @ApiModelProperty(value="action动作 这个暂时保留")
    private Byte action;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 用户名
     */
    @ApiModelProperty(value="角色id")
    private String roleId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSubSystemId() {
        return subSystemId;
    }

    public void setSubSystemId(Long subSystemId) {
        this.subSystemId = subSystemId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public Byte getMethod() {
        return method;
    }

    public void setMethod(Byte method) {
        this.method = method;
    }

    public String getOpenImg() {
        return openImg;
    }

    public void setOpenImg(String openImg) {
        this.openImg = openImg;
    }

    public String getCloseImg() {
        return closeImg;
    }

    public void setCloseImg(String closeImg) {
        this.closeImg = closeImg;
    }

    public Byte getResourceType() {
        return resourceType;
    }

    public void setResourceType(Byte resourceType) {
        this.resourceType = resourceType;
    }

    public Byte getLeaf() {
        return leaf;
    }

    public void setLeaf(Byte leaf) {
        this.leaf = leaf;
    }

    public Byte getAction() {
        return action;
    }

    public void setAction(Byte action) {
        this.action = action;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
