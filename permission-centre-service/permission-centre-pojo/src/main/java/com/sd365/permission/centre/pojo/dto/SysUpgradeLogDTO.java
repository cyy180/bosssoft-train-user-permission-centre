package com.sd365.permission.centre.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_upgrade_log")
public class SysUpgradeLogDTO extends TenantBaseDTO implements Serializable {

    @Transient
    private SubSystemDTO subSystemDTO;

    @Transient
    private TenantDTO tenantDTO;

    /**
     * 升级日志标题
     */
    private String title;

    /**
     * 升级日志内容
     */
    private String content;

    /**
     * 子系统id
     */
    private Long subSystemId;

    /**
     * 上一版本
     */
    private String oldVersion;

    /**
     * 这一版本
     */
    private String newVersion;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //接收时间类型
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss") //返回时间类型
    private Date upgradeTime;

    /**
     * 更新备注
     */
    private String upgradeRemark;

    /**
     * 升级操作人员
     */
    private String upgradeOperator;

}
