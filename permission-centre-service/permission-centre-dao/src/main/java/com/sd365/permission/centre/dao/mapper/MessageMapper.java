package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Message;
import com.sd365.permission.centre.pojo.query.MessageQuery;

import java.util.List;

public interface MessageMapper extends CommonMapper<Message> {

    /**
     * 通用查询带分页
     * @param messageQuery
     * @return
     */
    List<Message> commonQuery(MessageQuery messageQuery);
}
