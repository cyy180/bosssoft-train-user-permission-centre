package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;

import java.util.ArrayList;
import java.util.List;

public interface DepartmentMapper extends CommonMapper<Department> {
    /**
     * 通用查询带分页
     *
     * @param departmentQuery
     * @return
     */
    List<Department> commonQueryDepartment(DepartmentQuery departmentQuery);

    /**
     * 批量查询公司下
     * @param companyList
     * @return
     */
    List<Department> selectByCompanyList(ArrayList<Company> companyList);

    /**
     * 根据一级部门id查询下一级部门
     * @param id
     * @return
     */
    List<Department> selectByParentId(Long id);
    /**
     * 根据公司id查询一级部门
     * @param id
     * @return
     */
    List<Department> selectByCompanyId(Long id);

//    /**
//     * 添加部门
//     * @author Yan Huazhi
//     * @date 2020/12/18 10:48
//     * @version 0.0.1
//     * @param department
//     * @return
//     */
//    int insertDepartment(Department department);

}