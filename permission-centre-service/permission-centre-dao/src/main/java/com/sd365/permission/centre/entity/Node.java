/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName Node.java
 * @Author Administrator
 * @Date 2022-9-28  18:25
 * @Description 该文件定义了构建前端的树节点的结构，改文件结构有待改进应该能体现父子节点兄弟节点的关系
 * History:
 * <author> Administrator
 * <time> 2022-9-28  18:25
 * <version> 1.0.0
 * <desc> 构建树节点返回给前端
 */
package com.sd365.permission.centre.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.checkerframework.checker.units.qual.A;

/**
 * @Class Node
 * @Description 定义树节点，当前业务逻辑在前端处理不太好未来版本改进
 * @Author Administrator
 * @Date 2022-9-28  18:25
* @version 1.0.0
 */
@Data
@AllArgsConstructor
@ApiModel(value = "角色对应的资源树节点")
public class Node {
    private Long id;
    private String name;
}
