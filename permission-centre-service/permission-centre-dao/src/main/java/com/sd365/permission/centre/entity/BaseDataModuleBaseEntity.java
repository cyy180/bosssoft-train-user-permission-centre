package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class BaseDataModuleBaseEntity extends TenantBaseEntity {
    private Organization organization;
    private Company company;

    public BaseDataModuleBaseEntity(){
        this.organization=new Organization();
        this.company=new Company();
    }
}
