package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Organization;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrganizationMapper extends CommonMapper<Organization> {

    /**
     * 根据条件查询组织
     * @param organization
     * @return
     */
    List<Organization> commonQuery(Organization organization);
}