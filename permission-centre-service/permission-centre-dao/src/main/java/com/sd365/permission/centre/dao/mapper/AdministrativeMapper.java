package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Administrative;
import com.sd365.permission.centre.pojo.query.AdministrativeQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface AdministrativeMapper extends CommonMapper<Administrative> {
    /**
     * 查询区域数据
     */
    Administrative selectById(@Param("id") Long id);

    List<Administrative> commonQuery(AdministrativeQuery administrativeQuery);
}
