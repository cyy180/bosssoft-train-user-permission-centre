package com.sd365.permission.centre.service.util;


import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.ResourceService;
import com.sd365.permission.centre.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class InitRedis {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private RoleService roleService;


    @PostConstruct
    @Scheduled(cron = "0 0/30 * * * ?")
    public void initRedis() {
        try {
            ResourceQuery resourceQuery = new ResourceQuery();
            Role role = new Role();
            resourceService.initRedis(resourceQuery);
            roleService.initRoleResourceRedis(role);
            log.info("初始化成功");
        } catch (Exception e) {
            log.error("初始化失败");
            e.printStackTrace();
        }

    }
}
