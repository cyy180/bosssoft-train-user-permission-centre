package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.DictionaryType;
import com.sd365.permission.centre.pojo.vo.DictionaryCategoryVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>字典类型service</p>
 * @author 周靖赟
 * @version 1.0
 * @since  12/12/20
 */
public interface DictionaryCategoryService {

    /**
     * <p>查询字典类型</p>
     * @param dictionaryType 约束条件
     * @return 查询结果
     * */
    List<DictionaryCategoryVO> commonQuery(DictionaryType dictionaryType);

    /**
     * <p>根据id查询字典类型具体信息</p>
     * @param id id
     * @return 查询结果
     * */
    DictionaryCategoryVO info(@NotNull Long id);

    /**
     * <p>添加字典类型</p>
     * @param dictionaryType 字典类型实例
     * @return 是否成功
     * */
    Boolean add(DictionaryType dictionaryType);

    /**
     * <p>根据id和version修改字典类型</p>
     * @param dictionaryType 要修改的值
     * @return 是否修改成功
     * */
    Integer modify(DictionaryType dictionaryType);

    /**
     * <p>删除字典类型</p>
     * @param dictionaryType id 和 version
     * @return 是否删除成功
     * */
    Boolean delete(DictionaryType dictionaryType);

    /**
     * <p>批量删除字典类型</p>
     * @param dictionaryTypeList 字典类型列表
     * @return 是否删除成功
     * */
    Boolean batchDelete(List<DictionaryType> dictionaryTypeList);
}

