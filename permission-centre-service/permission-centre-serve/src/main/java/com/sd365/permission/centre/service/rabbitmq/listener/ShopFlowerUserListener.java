package com.sd365.permission.centre.service.rabbitmq.listener;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.sd365.common.core.mq.ActionType;
import com.sd365.common.core.mq.MqDataSyncMsg;
import com.sd365.permission.centre.pojo.dto.shop.ShopBuyerDTO;
import com.sd365.permission.centre.service.UserService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ConditionalOnProperty(prefix ="MyMqConfig",name ="shop",havingValue = "true")
@Component
public class ShopFlowerUserListener {
    @Autowired
    private UserService userService;


    public ShopFlowerUserListener(){

    }


    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(value = "shop_flower_queues"),
                    key = {"shop_flower_key"},
                    exchange = @Exchange(value = "shop_flower_exchange", type = "direct")
            )
    })
    public void shopUserReceiver(Channel channel, Message message) throws IOException {
        try {
            String body = new String(message.getBody(), "UTF-8");
            MqDataSyncMsg mqDataSyncMsg = JSON.parseObject(body, MqDataSyncMsg.class);
            List<Object> dataList = mqDataSyncMsg.getDataList();
            if (!CollectionUtils.isEmpty(dataList)) {

                ActionType actionType = mqDataSyncMsg.getActionType();
                if (actionType.name().equals("INSERT")) {
                    List<ShopBuyerDTO> shopBuyerDTOS = new ArrayList<>();
                    for (Object o : dataList) {
                        JSON.toJSONString(o);
                        ShopBuyerDTO shopBuyerDTO = JSON.parseObject(JSON.toJSONString(o), ShopBuyerDTO.class);
                        shopBuyerDTOS.add(shopBuyerDTO);
                    }
                    userService.shopUserRegister(shopBuyerDTOS);
                }
//                else if (actionType.name().equals("UPDATE")) {
//                    List<PcpUserDTO> pcpUserDTOS = new ArrayList<>();
//                    for (Object o : dataList) {
//                        JSON.toJSONString(o);
//                        PcpUserDTO pcpUserDTO = JSON.parseObject(JSON.toJSONString(o), PcpUserDTO.class);
//                        pcpUserDTOS.add(pcpUserDTO);
//                    }
//                    userService.pcpUserUpdate(pcpUserDTOS);
//                }else if (actionType.name().equals("DELETE")){
//                    List<PcpUserDTO> pcpUserDTOS = JSON.parseArray(JSON.toJSONString(dataList.get(0)), PcpUserDTO.class);
//                    userService.updateUserLockStatus(pcpUserDTOS);
//                }
            }
            else channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
        }
    }
}
