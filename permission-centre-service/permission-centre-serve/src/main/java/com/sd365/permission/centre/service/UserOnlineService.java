package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UserOnlineDTO;
import com.sd365.permission.centre.pojo.query.UserOnlineQuery;

import java.util.List;

/**
 * @Description: 用户在线管理服务
 * @Author: WengYu
 * @CreateTime: 2022/06/22 16:53
 */
public interface UserOnlineService {



    /**
     * 获取所有在线的用户
     * @return
     */
    List<UserOnlineDTO> getOnlineUsers(UserOnlineQuery userOnlineQuery);

    /**
     * 强制退出登陆
     * @param ids
     */
    void forceLogout(String ids);


    /**
     *  在用户登陆的时候保存用户信息
     */
    void saveLoginInfo(User user);
}
