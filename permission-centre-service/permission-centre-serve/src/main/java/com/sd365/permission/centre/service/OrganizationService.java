package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Organization;
import com.sd365.permission.centre.pojo.dto.OrganizationDTO;
import com.sd365.permission.centre.pojo.query.OrganizationQuery;
import com.sd365.permission.centre.pojo.vo.OrganizationStructureVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @className: OrganizationService
 * @description:
 * @author: 何浪
 * @date: 2020/12/11 17:59
 */
public interface OrganizationService {
    /**
     * 添加组织
     *
     * @param organizationDTO
     * @return
     */
    Boolean add(@RequestBody @Valid OrganizationDTO organizationDTO);

    /**
     * 删除组织
     *
     * @param id
     * @param version
     * @return
     */
    Boolean remove(Long id, Long version);


    /**
     * 修改组织
     *
     * @param organizationDTO
     * @return
     */
    Boolean modify(OrganizationDTO organizationDTO);

    /**
     * 根据条件查询组织
     *
     * @param organizationQuery
     * @return
     */
    List<OrganizationDTO> commonQuery(OrganizationQuery organizationQuery);


    /**
     * 根据id查询组织
     *
     * @param id
     * @return
     */
    OrganizationDTO queryById(Long id);

    /**
     * 复制一条和OrganizationDTO 一样的记录并且将值返回前端
     *
     * @param:
     * @return:
     * @see
     * @since
     */
    OrganizationDTO copy(Long id);

    /**
     * 批量删除组织
     *
     * @param organizationDTOS
     * @return
     */
    @Transactional
    Boolean batchDelete(OrganizationDTO[] organizationDTOS);

    /**
     * 批量修改组织
     *
     * @param organizationDTOS
     * @return
     */
    @Transactional
    Boolean batchUpdate(OrganizationDTO[] organizationDTOS);

    /**
     * 获取机构下的公司以及公司下部门
     * @param id
     * @return
     */
    List<OrganizationStructureVO> structureVoQueryById(Long id);
}
