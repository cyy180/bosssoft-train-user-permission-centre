package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.dto.DictionaryDTO;
import com.sd365.permission.centre.pojo.query.DictionaryQuery;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @class DictionaryService
 * @classdesc  字典服务接口
 * @author Administrator
 * @date 2020-10-18  1759
 * @version 1.0.0
 * @see
 * @since
 */
@Validated
public interface DictionaryService {
    /**
     * 增加字典记录
     * @param dictionaryDTO 字典dto对象 参考ui界面和表结构
     * @return true成功false就是失败
     */
    Boolean add(@Valid DictionaryDTO dictionaryDTO);
    /**
     *   删除字典记录，注意点就是如果字典被使用则由数据库的外键约束引发异常
     * @param id   记录
     * @param version  每次更新都会加1 id和版本维护这一行记录，是为了解决并发操作的问题是乐观所的一种机制
     * @return true成功false就是失败
     */
    Boolean remove(@NotNull Long id, @NotNull Long version);

    /**
     *  修改字典记录
     * @param dictionaryDTO 字典对象
     * @return true成功false就是失败
     */
    Boolean modify(@Valid  DictionaryDTO dictionaryDTO);

    /**
     * 批量修改字典记录
     * @param dictionaryDTOS 数组
     * @return  true成功false就是失败
     */
    @Transactional
    Boolean batchUpdate(@NotEmpty DictionaryDTO[] dictionaryDTOS);

    /**
     * 批量删除字典记录
     * @param dictionaryDTOS 批量删除的字段
     * @return true成功false就是失败
     */
    @Transactional
    Boolean batchRemove(@NotEmpty DictionaryDTO[] dictionaryDTOS);
    /**
     *  针对ui区域查询区的操作
     * @param dictionaryQuery  查询区的组合条件
     * @return 当前页字典列表
     */
    List<DictionaryDTO> commonQuery(@NotNull DictionaryQuery dictionaryQuery);
    /**
     *  根据id返回字典对象
     * @param id  字典id 一般是前端ui传递过来
     * @return  字典对象
     */
    @Cacheable(cacheNames = "dictionary",key ="#id")
    DictionaryDTO queryById(@NotNull  Long id);


    /**
     *  根据字典类别 返回该类别所包含的字典列表
     * @param id  字典类别id
     * @return 字典列表
     */
    @Cacheable(cacheNames = "dictionary",key ="#id")
    List<DictionaryDTO> queryByTypeId(@NotNull Long id);



}
