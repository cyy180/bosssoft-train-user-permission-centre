package com.sd365.permission.centre.service.util;

import cn.hutool.crypto.digest.MD5;

public class Md5Utils {
    public static String digestHex(String str) {
        MD5 md5 = MD5.create();
        return md5.digestHex(str);
    }
}
