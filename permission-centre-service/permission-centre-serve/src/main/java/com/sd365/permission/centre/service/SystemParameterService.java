package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.dto.DeleteSystemParamDTO;
import com.sd365.permission.centre.pojo.dto.SystemParamDTO;
import com.sd365.permission.centre.pojo.query.SystemParameterFuzzyQuery;
import com.sd365.permission.centre.pojo.query.SystemParameterQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface SystemParameterService {

    /**
     * @param: 区域DTO
     * @return: 成功则true
     * @see
     * @since
     */

//    @Cacheable(cacheNames = "systemParameterAddCache")
    Boolean add(@RequestBody @Valid SystemParamDTO systemParamDTO);


    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    Boolean remove(Long id, Long version);


    /**
     * @param:
     * @return:
     * @see
     * @since
     */
//    @Cacheable(cacheNames = "systemParameterModifyCache")
    Boolean modify(SystemParamDTO systemParamDTO);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */
//    @Cacheable(cacheNames = "systemParameterCommonQueryCache")
    List<SystemParamDTO> commonQuery(SystemParameterQuery systemParameterQuery);

    List<SystemParamDTO> fuzzyQuery(SystemParameterFuzzyQuery systemParameterFuzzyQuery);

    /**
     * @param: 区域id
     * @return: SystemParameter
     * @see
     * @since
     */
//    @Cacheable(cacheNames = "systemParameterQueryCache",key = "id")
    SystemParamDTO queryById(Long id);

    /**
     *  复制一条和SystemParameter 一样的记录并且将值返回前端
     * @param:
     * @return:
     * @see
     * @since
     */
//    @Cacheable(cacheNames = "systemParameterCopyCache")
    SystemParamDTO copy(Long id);

    @Transactional
    Boolean batchDelete(DeleteSystemParamDTO[] deleteSystemParamDTOS);
    @Transactional
    Boolean batchUpdate(SystemParamDTO[] systemParamDTOS);

    SystemParamDTO queryByName(String name);
}
