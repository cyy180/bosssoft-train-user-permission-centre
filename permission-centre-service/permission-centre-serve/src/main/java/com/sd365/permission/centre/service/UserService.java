package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.entity.Position;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UserCentreDTO;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import com.sd365.permission.centre.pojo.dto.pcp.PcpUserDTO;
import com.sd365.permission.centre.pojo.dto.shop.ShopBuyerDTO;
import com.sd365.permission.centre.pojo.query.UserQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
/**
 * @author yangshaoqi
 * @version 1.0.0
 * @class UserService
 * @classdesc 维护用户信息的服务接口
 * @date 2020-12-14  21点28分
 */
public interface UserService {
    /**
     * 增加用户
     * @param: 商品DTO
     * @return: 成功则true
     */
    @Transactional
    Boolean add(@Valid @RequestBody UserDTO userDTO);


    /**
     * 根据id和版本删除用户
     * @param: id与版本
     * @return: 成功则true
     */
    @Transactional
    Boolean remove(Long id, Long version);


    /**
     * @param: 商品DTO
     * @return: 成功则true
     * @see
     * @since
     */
    @Transactional
    Boolean modify(UserDTO userDTO);

    /**
     * @Description:门户更新用户信息方法 新增方法 解决原来的更新调用导致无法登录问题，
     *  注意原来是调用老方法 因为不方便修改老方法逻辑所以改为新增方法
     * @Author: 郑黄彬
     * @DATE: 2022-12-7  11:33
     * @param: userCentreDTO  BM 透传过来用户个人信息
     * @return: true 更新成功  false失败
     */
    Boolean modifyUserInfo(UserCentreDTO userCentreDTO);

    /**
     * @Description: 业务管理平台更新用户信息方法
     * @Author: Administrator
     * @DATE: 2022-12-7  11:37
     * @param:
     * @return:
     */
    @Transactional
    Boolean modifyUserInfoForBm(UserCentreDTO userCentreDTO);

    /**
     * @param: 商品DTO
     * @return: 成功则true
     * @see
     * @since
     */
    @Transactional
    Boolean modifyWithNewRole(UserDTO[] userDTOS);

    /**
     * @param: 用户搜索条件
     * @return: 用户角色列表
     * @see
     * @since
     */

    List<User> commonQuery(UserQuery userQuery);

    /**
     * @param: 用户id
     * @return: user
     * @see
     * @since
     */
    User queryById(Long id);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    List<Role> queryAllRole();
    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    List<Department> queryAllDepartment();
    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    List<Position> queryAllPosition();



    /**
     *  批量更新商品记录
     * @param userDTOS
     * @return
     */
    @Transactional
    Boolean batchUpdate(UserDTO[] userDTOS);


    /**
     * 批量删除商品记录
     * @param userDTOS
     * @return
     */
    @Transactional
    Boolean batchDelete(UserDTO[] userDTOS);


    Boolean firstStartMd5();



    /**
     * 换电用户注册
     * @param
     * @return
     */
    @Transactional
    Boolean pcpUserRegister(List<PcpUserDTO> pcpUserDTOS);

    /**
     * 换电用户密码修改
     * @param
     * @return
     */
    @Transactional
    Boolean pcpUserUpdate(List<PcpUserDTO> pcpUserDTOS);

    @Transactional
    Boolean updateUserLockStatus(List<PcpUserDTO> pcpUserDTOS);



    Boolean updateUserLockStatus(PcpUserDTO userDTO);






    /**
     * 花店用户注册
     * @param
     * @return
     */
    @Deprecated
    @Transactional
    Boolean shopUserRegister(List<ShopBuyerDTO> shopBuyerDTOS);

    Boolean updatePassword(UserDTO userDTO);

    /**
     * code 查询用户信息
     * @param code
     * @return
     */
    User getUser(String code );
}
