package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @Auther: zhengkongjin
 * @Date: 2020/12/11/ 22:05
 * @Description:
 */
public interface TenantService {
    /**
     * @param tenantDTO
     * @return 成功则true
     */
    Boolean add(@RequestBody @Valid TenantDTO tenantDTO);

    /**
     * @param tenantQuery
     * @return
     */
    List<TenantDTO> commonQuery(TenantQuery tenantQuery);

    /**
     * @param id
     * @param version
     * @return
     */
    Boolean remove(Long id,Long version);

    /**
     * @param tenantDTO
     * @return
     */
    Boolean modify(TenantDTO tenantDTO);

    /**
     * @param deleteTenantDTOS
     * @return
     */
    @Transactional
    Boolean batchDelete(DeleteTenantDTO[] deleteTenantDTOS);

    /**
     * @param id
     * @return
     */
    TenantDTO queryById(Long id);

    /**
     * @param name
     * @return
     */
    TenantDTO queryByName(String name);
}
