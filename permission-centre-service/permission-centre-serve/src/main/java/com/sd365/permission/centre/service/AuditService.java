package com.sd365.permission.centre.service;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.permission.centre.entity.Audit;
import com.sd365.permission.centre.pojo.dto.AuditDTO;
import com.sd365.permission.centre.pojo.query.AuditQuery;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class AuditService
 * @Description 审计管理服务类，负责对自定义的事件进行审计记录，例如 登录，修改重要数据等
 * @Author Administrator
 * @Date 2023-02-20  19:47
 * @version 1.0.0
 */
@Validated
public interface AuditService {
    /**
     * 查询审计记录 包含分页数据
     * @param: 审计搜索条件 分页查询
     * @return: 审计列表
     */
    @Pagination
    List<Audit> commonQuery(@NotNull AuditQuery auditQuery);

    /**
     * 查询审计记录
     * @param id 记录id
     * @return  审计记录DTO对象
     */
    AuditDTO queryById(@NotNull Long id);

    /**
     * 查询密码过于简单的记录
     * @param isusp
     * @return 返回 审计记录
     */
    List<AuditDTO> selectByUseSimplePwd(@NotNull Byte isusp);

    /**
     *  查询密码修改记录
     * @param isnup
     * @return 审计记录
     */
    List<AuditDTO> selectByNoUpdatePwd(@NotNull Byte isnup);

    /**
     *  查询长时间没登录用户
     * @param islnl
     * @return  审计记录
     */
    List<AuditDTO> selectByLongNoLogin(@NotNull Byte islnl);
}
