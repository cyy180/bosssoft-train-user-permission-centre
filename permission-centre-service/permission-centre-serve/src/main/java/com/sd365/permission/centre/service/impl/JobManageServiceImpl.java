package com.sd365.permission.centre.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.core.mq.ActionType;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.JobMapper;
import com.sd365.permission.centre.entity.JobManage;
import com.sd365.permission.centre.pojo.dto.JobDTO;
import com.sd365.permission.centre.pojo.query.JobQuery;
import com.sd365.permission.centre.service.JobManageService;
import com.sd365.permission.centre.service.exception.UserCentreExceptionCode;
import com.sd365.permission.centre.service.util.SendMQMessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Service
public class JobManageServiceImpl extends AbstractBaseDataServiceImpl implements JobManageService {
    @Resource
    JobMapper jobMapper;

    @Autowired
    private SendMQMessageUtil sendMQMessageUtil;
    private static final String TABLE_NAME = "basic_job_manage";

    @Autowired
    private IdGenerator idGenerator;

    @Override
    public List<JobDTO> commonQueryJob(JobQuery jobQuery) {
        if (jobQuery == null) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }

            List<JobManage> jobEntities = jobMapper.commonQueryJob(jobQuery);
            Page page = (Page) jobEntities;
            BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            List<JobDTO> jobDTOS = BeanUtil.copyList(jobEntities, JobDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可为空", new Exception("copyList 拷贝回调错误"));
                    }
                    JobManage jobManageSource = (JobManage) o;
                    JobDTO jobDTO = (JobDTO) o1;
                }
            });
            return jobDTOS;
    }


    @Override
    public Boolean add(@Valid JobDTO jobDTO) {
        JobManage jobManage = BeanUtil.copy(jobDTO, JobManage.class);
        jobManage.setTenantId(BaseContextHolder.getTanentId());
        jobManage.setId(idGenerator.snowflakeId());
        jobManage.setVersion(1L);
        baseDataStuff4Add(jobManage);

        Boolean flag=jobMapper.insert(jobManage)>0;
        if (flag) {
            sendMQMessageUtil.SendMessage(ActionType.UPDATE, SendMQMessageUtil.EXCHANGE_NAME, TABLE_NAME, jobManage);
        }
        return flag;
    }
    @Transactional
    @Override
    public Boolean remove(Long id,Long version) {
        Example example = new Example(JobManage.class);
        example.createCriteria().andEqualTo("id", id).andEqualTo("version",version);
            Boolean flag = jobMapper.deleteByExample(example) > 0;
            if (flag) {
                JobManage jobManage = new JobManage();
                jobManage.setId(id);
                jobManage.setVersion(version);
                sendMQMessageUtil.SendMessage(ActionType.DELETE, SendMQMessageUtil.EXCHANGE_NAME, TABLE_NAME, jobManage);
            }
            return flag;

    }


    @Override
    public Boolean modify(JobDTO jobDTO) {
        JobManage jobManage = BeanUtil.copy(jobDTO, JobManage.class);
        jobManage.setTenantId(BaseContextHolder.getTanentId());
        baseDataStuff4Updated(jobManage);
        Example example = new Example(JobManage.class);
        example.createCriteria().andEqualTo("id", jobDTO.getId()).andEqualTo("version", jobDTO.getVersion());
        jobDTO.setUpdatedTime(new Date());
        Boolean flag = jobMapper.updateJob(jobManage) > 0;
        if (flag) {
            sendMQMessageUtil.SendMessage(ActionType.UPDATE, SendMQMessageUtil.EXCHANGE_NAME, TABLE_NAME, jobManage);
        }
        return flag;
    }

    @Override
    public void resumeJobStatus(Long id) {
        jobMapper.resumeJobStatus(id);
    }

    @Override
    public void pauseJobStatus(Long id) {
        jobMapper.pauseJobStatus(id);
    }
}
