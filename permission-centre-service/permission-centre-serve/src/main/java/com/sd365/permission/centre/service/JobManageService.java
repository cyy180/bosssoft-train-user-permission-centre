package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.dto.JobDTO;
import com.sd365.permission.centre.pojo.query.JobQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface JobManageService {
    /**
     * 查询任务
      * @param jobQuery
     * @return
     */
    List<JobDTO> commonQueryJob(JobQuery jobQuery);

    /**
     * 增加任务
     * @param jobDTO
     * @return
     */
    Boolean add(@RequestBody @Valid JobDTO jobDTO);

    /**
     * 删除任务
     * @param id
     * @return
     */
    Boolean remove(Long id,Long version);

    /**
     * 修改任务
     * @param jobDTO
     * @return
     */
    @Transactional
    Boolean modify(JobDTO jobDTO);

    /**
     * 启动任务后，修改任务状态为1
     * @return
     */
    void resumeJobStatus(Long id);

    /**
     * 暂停任务后，修改任务状态为0
     * @return
     */
    void pauseJobStatus(Long id);

}
