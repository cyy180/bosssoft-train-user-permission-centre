package com.sd365.permission.centre.service;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.permission.centre.entity.*;
import com.sd365.permission.centre.pojo.dto.AdministrativeDTO;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.query.AdministrativeQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class AdministrativeService
 * @Description 区划服务接口
 * @Author Administrator
 * @Date 2023-02-14  17:18
 * @version 1.0.0
 */
@Validated
public interface AdministrativeService {

    /** 增加区划记录
     * @param: 区域DTO
     * @return: 成功则true
     */
    Boolean add(@Valid AdministrativeDTO administrativeDTO);


    /** 删除区划记录
     * @param: id
     * @return: 成功则true
     */
    Boolean remove(@NotNull Long id,@NotNull Long version);

    /** 修改区划记录
     * @param: 区域DTO
     * @return: 成功则true
     */
    Boolean modify(@Valid  AdministrativeDTO administrativeDTO);
    /**
     *  按区域名称和助记码查找区划  采用新注解 @Pagination 实现分页
     * @param: administrativeQuery 区域搜索条件
     * @return: 区域VO对象列表
     */
    @Pagination
    List<Administrative> commonQuery(@NotNull AdministrativeQuery administrativeQuery );

    /**
     *  根据id返回区划对象
     * @param id 区划记录id
     * @return 区划DTO 对象
     */
    AdministrativeDTO queryById(@NotNull Long id);

    /**
     * 批量删除区域记录
     * @param idVersionQueryList
     * @return
     */
    @Transactional
    Boolean batchDelete(@NotEmpty List<IdVersionQuery> idVersionQueryList);

}
