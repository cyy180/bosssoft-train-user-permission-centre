package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.pojo.vo.ResourceVO;

import java.util.List;

/**
 * @author No_one
 * @description 用于根据资源生成菜单，菜单即资源的层级关系，ResourceVO中包含List<ResourceVO>，代表子结点
 */
public interface MenuService {

    /**
     * 根据角色的资源生成菜单
     * @param resourceIdList
     * @return
     */
    public List<ResourceVO> buildMenu(List<Resource> resourceIdList);
}
