package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.dto.DeleteSystemParamDTO;
import com.sd365.permission.centre.pojo.dto.SystemParamDTO;
import com.sd365.permission.centre.pojo.query.SystemParameterFuzzyQuery;
import com.sd365.permission.centre.pojo.query.SystemParameterQuery;
import com.sd365.permission.centre.pojo.vo.SystemParamVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Digi_yzq
 */
@Api(tags = "参数管理 ",value ="/permission/centre/v1/system/parameter")
@RequestMapping(value = "/permission/centre/v1/system/parameter")
@CrossOrigin
public interface SystemParameterApi {

    /**
     * @param: systemParameterDTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags="增加参数",value="")
    @PostMapping(value="/add")
    @ResponseBody
    Boolean add(@RequestBody @Valid SystemParamDTO systemParamDTO);

    /**
     * @param: id,version
     * @return: 成功则true
     * @see
     * @since
     */
    @ApiOperation(tags="删除参数",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * @param: systemParameterDTOList
     * @return: 成功则true，批量删除
     * @see
     * @since
     */
    @ApiOperation(tags="批量删除参数",value="/remove")
    @PostMapping(value="/remove")
    @ResponseBody
    Boolean batchRemove(@RequestBody @Valid DeleteSystemParamDTO[] deleteSystemParamDTOS);

    @ApiOperation(tags="批量修改客户",value="")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdate( @Valid @RequestBody  SystemParamDTO[] systemParamDTOS);
    /**
     * @param: systemParameterDTO
     * @return: 成功则true
     * @see
     * @since
     */
    @ApiOperation(tags="修改参数",value="")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody SystemParamDTO systemParameDTO);

    /**
     *  系统框架GlobalControllerResolver 类分析 参数是否 BaseQuery 类型 ，如果是则 拦截调用
     *  <br> PageHelper 分页方法， 并且将返回的page对象放入TheadLocal ，方法返回参数被 ResponseBodyAware拦截
     *  <br> 其判断 返回值的类型 如果是属于分页的请求则 自动将 List<system/parameterVO> 装入CommonPage
     *  <br> 并且构建统一应答回去 以上改进优化了 请求和应答的方法的编写
     *
     * @param: systemParameterQuery
     * @return: 查询列表list
     * @see
     * @since
     */

    @ApiOperation(tags="查询参数",value="")
    @GetMapping(value = "")
    @ResponseBody
    List<SystemParamVO> commonQuery(SystemParameterQuery systemParameterQuery);

    /**
     * @param: SystemParameterFuzzyQuery
     * @return:  查询列表list
     * @description 解决 commonQuery 参数类型仅能通过id查询的问题
     */
    @ApiOperation(tags="模糊查询参数",value="/fuzzy")
    @GetMapping(value = "/fuzzy")
    @ResponseBody
    List<SystemParamVO> fuzzyQuery(SystemParameterFuzzyQuery systemParameterFuzzyQuery);

    /**
     * @param: id
     * @return:  SystemParameterVO
     * @see
     * @since
     */
    @ApiOperation(tags="查询参数通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    SystemParamVO querySystemParameterById(@PathVariable("id") Long id);

    @ApiOperation(tags = "查询是否存在仓库名", value="")
    @GetMapping(value="/queryByName")
    @ResponseBody
    SystemParamVO QueryByName(String name);
}
