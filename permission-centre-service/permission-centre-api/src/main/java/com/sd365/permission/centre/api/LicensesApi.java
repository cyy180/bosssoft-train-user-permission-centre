package com.sd365.permission.centre.api;


import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.pojo.vo.LicensesVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 授权与许可
 */
@CrossOrigin
@Api(tags = "授权与许可",value = "/permission/centre/v1/licenses")
@RequestMapping(value = "/permission/centre/v1/licenses")
public interface LicensesApi {

    /**
     * 查询配置信息
     * @return 将配置信息封装在licensesVO对象中返回
     */
    @ApiOperation(tags="查询配置文件信息",value="")
    @GetMapping("")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    List<LicensesVO> showInfo();
}
