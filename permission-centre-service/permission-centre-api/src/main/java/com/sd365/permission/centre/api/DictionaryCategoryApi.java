package com.sd365.permission.centre.api;

import com.sd365.common.core.common.api.CommonResponse;
import com.sd365.permission.centre.pojo.dto.DictionaryTypeDTO;
import com.sd365.permission.centre.pojo.query.DictionaryCategoryQuery;
import com.sd365.permission.centre.pojo.vo.DictionaryCategoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>字典类型 api</p>
 * @author 周靖赟
 * @version 1.0
 * @since  12/12/20
 */

@Api(value = "/permission/centre/dictionary-type", tags = "字典类型管理")
@CrossOrigin
@RequestMapping("/permission/centre/v1/dictionary-type")
public interface DictionaryCategoryApi {
    /**
     * <p>查询字典类型</p>
     * @param query 查询条件
     * @return 查询结果
     * */
    @ApiOperation(value = "查询字典类型", httpMethod = "GET")
    @RequestMapping(value = "", method = {RequestMethod.GET})
    @ResponseBody
    List<DictionaryCategoryVO> commonQuery(DictionaryCategoryQuery query);

    /**
     * <p>根据id查询字典类型具体信息</p>
     * @param id id
     * @return 查询结果
     * */
    @ApiOperation(value = "根据 id 查询字典类型具体信息", httpMethod = "GET")
    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    DictionaryCategoryVO info(@PathVariable("id") Long id);

    /**
     * <p>添加字典类型</p>
     * @param dictionaryCategory 字典类型实例
     * @return 是否成功
     * */
    @ApiOperation(value = "添加字典类型", httpMethod = "POST")
    @RequestMapping(value = "", method = {RequestMethod.POST})
    @ResponseBody
    Boolean save(@RequestBody DictionaryTypeDTO dictionaryCategory);

    /**
     * <p>根据id和version修改字典类型</p>
     * @param dictionaryCategory 要修改的值
     * @return 是否修改成功
     * */
    @ApiOperation(value = "修改字典类型", httpMethod = "PUT")
    @RequestMapping(value = "", method = {RequestMethod.PUT})
    @ResponseBody
    CommonResponse<Boolean> update(@RequestBody DictionaryTypeDTO dictionaryCategory);

    /**
     * <p>删除字典类型</p>
     * @param dictionaryCategory id 和 version
     * @return 是否删除成功
     * */
    @ApiOperation(value = "删除字典类型", httpMethod = "DELETE")
    @RequestMapping(value = "", method = {RequestMethod.DELETE})
    @ResponseBody
    CommonResponse<Boolean> delete(@RequestBody DictionaryTypeDTO dictionaryCategory);

    /**
     * <p>批量删除字典类型</p>
     * @param dictionaryCategoryList 字典类型列表
     * @return 删除结果
     * */
    @ApiOperation(value = "批量删除字典类型", httpMethod = "DELETE")
    @RequestMapping(value = "/batch", method = {RequestMethod.DELETE})
    @ResponseBody
    CommonResponse<Boolean> batchDelete(@RequestBody List<DictionaryTypeDTO> dictionaryCategoryList);
}
