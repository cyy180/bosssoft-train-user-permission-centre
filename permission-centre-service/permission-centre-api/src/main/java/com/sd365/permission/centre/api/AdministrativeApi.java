package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.dto.AdministrativeDTO;
import com.sd365.permission.centre.pojo.query.AdministrativeQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.vo.AdministrativeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class AdministrativeApi
 * @Description 行政区域化用于做B端的项目的区域划分
 * @Author Administrator
 * @Date 2023-02-14  9:25
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "行政区划 ", value = "/permission/centre/v1/administrative")
@RequestMapping(value = "/permission/centre/v1/administrative")
public interface AdministrativeApi {
    /**
     * 增加区划
     * @param: 区域DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags = "增加区域", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@Valid @RequestBody  AdministrativeDTO administrativeDTO);


    /**
     *  删除区化
     * @param: id  记录id
     * @param: version 记录的版本记录 ，因为采用乐观所所以传该参数
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "删除区域", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @NotNull @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @NotNull @RequestParam("version") Long version);

    /** 修改
     * @param: 区域DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "修改区域", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody AdministrativeDTO administrativeDTO);

    /**
     * @param: 区域查询条件
     * @return: 用户VO
     */

    @ApiOperation(tags = "查询区域", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<AdministrativeVO> commonQuery(@NotNull  AdministrativeQuery administrativeQuery);

    /**
     * @param: 区域ID
     * @return: 区域VO
     */
    @ApiOperation(tags="查询区域通过id",value="")
    @GetMapping(value = "/{id}")
    @ResponseBody
    AdministrativeVO selectById(@NotNull @PathVariable("id") Long id);


    /**
     * @param: 用户DTO数组
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags="批量删除区域",value="")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    Boolean batchDelete( @NotEmpty @RequestBody  List<IdVersionQuery> idVersionQueryList);
    /**
     * 更新单条启用状态
     * @param administrativeDTO
     * @return
     */
    @ApiOperation(tags = "修改状态", value = "/status")
    @PutMapping(value = "/status")
    @ResponseBody
    Boolean status(@Valid @RequestBody AdministrativeDTO administrativeDTO);
}
