/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: LoginApi
 * Author: Administrator
 * Date: 2022-10-8 18:07:44
 * Description:
 * <p> 重构了LoginController接口 新增加了接口给LoginController实现重构了RequestMapping URL
 * History:
 * <author> Administrator
 * <time> 2022-10-8 18:07:44
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.api;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @InterfaceName: LoginApi
 * @Description: 控制器实现此接口，该接口主要是提供登录服务，主要的调用者来自网关
 * @Author: Administrator
 * @Date: 2022-10-08 18:07
 **/
@RequestMapping(value = "/permission/centre/v1")
public interface LoginApi {
    /**
     * @Class LoginController
     * @Description 内部类封装在UserVO中的用于返回角色资源信息
     * @Author Administrator
     * @Date 2022-10-8  15:31
     * @version 1.0.0
     */
    @Data
    class ResponseData {
        /**
         * 生成的token ，token在认证服务生成比较合理
         */
        private String token;
        /**
         *  一个用户可以拥有多个角色这里是id
         */
        private List<String> roles;
        /**
         *  名字
         */
        private String name;
        /**
         * 头像地址
         */
        private String avatar;
        /**
         *  用户所拥有的资源，如果在 Role对象中包含这个会更好
         */
        private List<ResourceVO> resourceVO;
    }
    @Data
    public class UserVO {
        /**
         * 名字
         */
        private String name;
        /**
         * 电话号码
         */
        private String tel;
        /**
         * 主键雪花算法生成
         */
        private Long id;
        /**
         * 消息和错误码 这个设计不号，返回保卫已经被统一应答包装 CommonResponse的Head包含了 code和message 这里不需要了
         */
        private String msg;
        private int code;

        private Long tenantId;
        private String companyAddress;
        private Long orgId;
        private Long companyId;
        public ResponseData data;
        private List<Long> roleIds;
    }
    /**
     * @Description: 控制层不应该包括业务逻辑，该方法需要被优化，多数的业务逻辑迁移到LoginService 类
     *   前端发起的 permission/centre/v1/user/login 但是在请求在 网关配置被转发到 认证
     * - id: gateway-authentication-service
     *  uri: lb://gateway-authentication
     *  predicates:
     *- Path=/permission/centre/v1/user/login
     * filters:
     * - RewritePath=/permission/centre/v1/user/login, /auth/token
     * @Author: Administrator
     * @DATE: 2022-10-12  14:24
     * @param: 认证服务传递过来的 账号，密码，租户号
     * @return: 认证成功的用户信息内部包含数据 ，认证失败则返回对象但是对象内部无数据，前端需要依据UserVO内部数据判断
     *  TODO 开发人员完成该方法的认证实现，具体的需要返回的信息根据 网关和前端部门的理解处理
     */
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/auth")
    @ResponseBody
    UserVO auth(@RequestParam(value = "code") String code, @RequestParam(value = "password") String password, @RequestParam(value = "account") String account);

    /**
     * @Description: 前端认证完成后调用此方法获取用户信息，用户信息包含权限信息
     * @Author: Administrator
     * @DATE: 2022-10-8  18:11
     * @param: code 工号，account 租户账号
     * @return: 用户认证通过后需要调用该方法返回 UserVO 其中包含资源菜单
     * TODO 开发人员完成该方法的实现
     */
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/info")
    @ResponseBody
    UserVO info(@RequestParam(value = "code") String code, @RequestParam(value = "account") String account);

    /**
     * @Description: 原业务逻辑写在controller中后改为调用UserOnlineService
     * @Author: Administrator
     * @DATE: 2022-10-13  11:14
     * @return: 返回值只是为了满足接口前端接口的需要
     */
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/logout")
    @ResponseBody
    UserVO logout();
}
