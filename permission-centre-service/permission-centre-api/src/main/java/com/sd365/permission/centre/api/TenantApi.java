package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Auther: zhengkongjin
 * @Date: 2020/12/11/  18:55
 * @Description: 租户管理 api
 */
@CrossOrigin
@Api(tags = "租户管理",value = "/permission/centre/v1/tenant")
@RequestMapping(value = "/permission/centre/v1/tenant")
public interface TenantApi {

    /**
     * @param tenantDTO
     * @return 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags="增加租户",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@RequestBody @Valid TenantDTO tenantDTO);

    /**
     * @param tenantQuery
     * @return
     */
    @ApiOperation(tags="查询租户",value="")
    @GetMapping(value = "")
    @ResponseBody
    List<TenantVO> commonQuery(TenantQuery tenantQuery);

    /**
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除租户",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * @param tenantDTO
     * @return
     */
    @ApiOperation(tags="修改租户",value="")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid  TenantDTO tenantDTO);

    @ApiOperation(tags="批量删除租户",value="/batch")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchRemove(@Valid @RequestBody DeleteTenantDTO[] deleteTenantDTOS);

    /**
     * @param id
     * @return
     */
    @ApiOperation(tags="查询租户通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    TenantVO queryTenantById(@PathVariable("id") Long id);

    @ApiOperation(tags = "查询是否存在租户名", value="")
    @GetMapping(value = "/queryByName")
    @ResponseBody
    TenantVO queryByName(String name);
}
