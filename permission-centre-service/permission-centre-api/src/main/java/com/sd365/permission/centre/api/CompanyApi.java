package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.query.CompanyQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.vo.CompanyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class CompanyApi
 * @Description 公司基础数据管理接口
 * @Author Administrator
 * @Date 2023-02-20  21:59
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "公司管理 ",value ="/permission/centre/v1/company")
@RequestMapping(value = "/permission/centre/v1/company")
public interface CompanyApi {

    /**
     * 通用查询
     * @param companyQuery
     * @return 公司列表
     */
    @ApiOperation(tags = "查询公司信息，分页", value = "")
    @GetMapping("")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    List<CompanyVO> commonQuery(CompanyQuery companyQuery);

    /**
     * 批量更新公司数据
     * @param companyDTOS
     * @return true成功false失败
     */
    @ApiOperation(tags="批量更新",value="/batch")
    @PutMapping(value = "/batch")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    Boolean batchUpdate(@NotEmpty @RequestBody List<CompanyDTO> companyDTOS);

    /** 插入公司对象
     * @param: 公司DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "增加公司信息", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@Valid  @RequestBody CompanyDTO companyDTO);
    /**
     *  删除公司对象
     * @param id  公司记录id
     * @param version 公司记录版本
     * @return true成功false失败
     */
    @ApiOperation(tags = "删除公司信息", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam(value = "id", required = true) Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam(value = "version", required = true) Long version);

    /**
     *  批量删除公司
     * @param idVersionQueryList  列表元素包含id和version
     * @return true成功 false失败
     */
    @ApiOperation(tags = "批量删除公司信息", value = "/batch")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.DELETE)
    Boolean removeBatch(@NotEmpty @RequestBody List<IdVersionQuery> idVersionQueryList);

    /**
     * 修改公司信息
     * @param companyDTO 公司数据
     * @return true成功false失败
     */
    @ApiOperation(tags = "修改公司信息", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@Valid @RequestBody CompanyDTO companyDTO);

    /**
     * 根据id查找公司对象
     * @param id  记录id
     * @return 公司对象
     */
    @ApiOperation(tags = "通过id查询", value = "")
    @GetMapping(value = "{id}")
    @ResponseBody
    CompanyVO queryCompanyById(@NotNull @PathVariable("id") Long id);
    /**
     *  拷贝接口先返回公司对象然后点确保就调用添加保存
     * @param id 当前被拷贝的记录的id
     * @return 当前被拷贝记录对象
     */
    @ApiOperation(tags = "通过id查询", value = "/copy/{id}")
    @GetMapping(value = "/copy/{id}")
    @ResponseBody
    CompanyDTO copy(@NotNull @PathVariable("id") Long id);
    /**
     *  修改公司状态例如更新可用和不可用
     * @param companyDTO 公司对象
     * @return true成功 false失败
     */
    @ApiOperation(tags = "通过id查询", value = "/status")
    @PutMapping(value = "/status")
    @ResponseBody
    Boolean status(@RequestBody CompanyDTO companyDTO);
}
